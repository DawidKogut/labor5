package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class  Operations implements StackOperations {
    private final ArrayList<String> stack;

    public Operations() {
        this.stack = new ArrayList<>();
    }

    @Override
    public List<String> get() {
        return stack;
    }

    @Override
    public Optional<String> pop() {
        if(stack.isEmpty()) {
            return Optional.empty();
        }
        Optional<String> lastItem = Optional.of(stack.get(stack.size() - 1));
        stack.remove(stack.size() - 1);
        return lastItem;
    }

    @Override
    public void push(String item) {
        stack.add(item);
    }
}