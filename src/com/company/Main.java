package com.company;
public class Main {

    public static void main(String[] args) {
        Operations stack = new Operations();

        stack.push("a");
        stack.push("l");
        stack.push("a");
        stack.push(" ");
        stack.push("m");
        stack.push("a");

        System.out.println(stack.get());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.get());
    }
}